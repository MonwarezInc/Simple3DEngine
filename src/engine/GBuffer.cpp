/*
Copyright (c) 2016, Payet Thibault
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Monwarez Inc nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL PAYET THIBAULT BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "GBuffer.h"

#include <sstream>
#include <string>

#include <range/v3/all.hpp>

using namespace S3DE;

GBuffer::GBuffer( unsigned int width, unsigned int height )
{
    init_( width, height );
}

GBuffer::~GBuffer()
{
    if ( glIsFramebuffer( m_fbo ) )

    {
        glDeleteFramebuffers( 1, &m_fbo );
    }

    glDeleteTextures( static_cast<size_t>( GBufferTextureType::NUM_TEXTURES ), m_textures.data() );
}

GBuffer::GBuffer( GBuffer const& buffer )
{
    //
    throw std::runtime_error( "Copy not implemented yet" );
}

GBuffer& GBuffer::operator=( GBuffer const& buffer )
{
    throw std::runtime_error( "Not implemented yet" );
}

void GBuffer::bindForWriting()
{
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, m_fbo );
}

void GBuffer::bindForReading()
{
    glBindFramebuffer( GL_READ_FRAMEBUFFER, m_fbo );
}

void GBuffer::init_( unsigned int width, unsigned int height )
{
    // First create the FBO
    glGenFramebuffers( 1, &m_fbo );
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, m_fbo );

    // Create gbuffer textures
    glGenTextures( static_cast<int>( GBufferTextureType::NUM_TEXTURES ), m_textures.data() );
    glGenTextures( 1, &m_depthTexture );

    for ( auto const& [ texture, attachment ] :
          ranges::views::zip( m_textures, drawBuffersAttachments_ ) )
    {
        glBindTexture( GL_TEXTURE_2D, texture );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, nullptr );
        glFramebufferTexture2D( GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture, 0 );
    }
    // Depth
    glBindTexture( GL_TEXTURE_2D, m_depthTexture );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, width, height, 0, GL_DEPTH_COMPONENT,
                  GL_FLOAT, nullptr );
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthTexture, 0 );

    glDrawBuffers( static_cast<size_t>( GBufferTextureType::NUM_TEXTURES ),
                   drawBuffersAttachments_.data() );

    GLenum Status = glCheckFramebufferStatus( GL_FRAMEBUFFER );

    if ( Status != GL_FRAMEBUFFER_COMPLETE )
    {
        std::stringstream out;
        out << "Error at " << __FILE__ << "(" << __LINE__ << ") \n";
        out << "Framebuffer error, status: 0x" << Status << '\n';
        throw out.str();
    }

    // Then restore default framebuffer
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
}
