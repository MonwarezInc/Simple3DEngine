#include "Engine-main.h"

namespace S3DE
{
Engine::Engine( Scene &sceneView )
    : scene_{ sceneView }
{
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_CULL_FACE );
    glCullFace( GL_BACK );
}


auto Engine::registerShader( const EngineShader &engineShader ) -> void
{
    scene_.registerShader( engineShader.lightV, engineShader.lightF, engineShader.lightG );
}

void Engine::ClearColor( float r, float g, float b, float a )
{
    glClearColor( r, g, b, a );
}


void Engine::Draw( std::chrono::duration<float, std::chrono::seconds::period> elapsed )
{
    scene_.draw( elapsed );
}
} // namespace S3DE
