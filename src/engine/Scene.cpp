#include "Scene.h"

#include "MeshException.h"

namespace S3DE
{
auto Scene::loadMeshes( std::vector<MeshPair> const &meshPair ) -> void
{
    m_entToID.clear();
    m_vObjectNode.clear();
    m_meshManager.Load( meshPair );
    for ( auto &v : meshPair )
        addMeshNode_( v.entity );
}

auto Scene::addMeshNode_( std::string const &entity, bool isVisible ) -> void
{
    ObjectNode objectNode;
    objectNode.entity   = entity;
    objectNode.position = glm::vec3( 0.0, 0.0, 0.0 );
    objectNode.scale    = 1.0;
    for ( unsigned int i = 0; i < 3; ++i )
        objectNode.pitch[ i ] = 0.0;
    objectNode.animation = "idle"; // idle by default
    objectNode.id        = m_vObjectNode.size();
    objectNode.isVisible = isVisible;
    m_vObjectNode.push_back( objectNode );
    m_entToID[ entity ] = objectNode.id;
}
auto Scene::delMeshNode( std::string const &entity ) -> void
{
    // If succeed does not launch an exception
    // it's not manage the memory of the mesh itself
    auto it = m_entToID.find( entity );
    if ( it == m_entToID.end() )
        throw std::string( "Error the id is to high for DelMeshNode" );
    m_vObjectNode[ it->second ].isVisible = false; // Set to false so that we didn't show it
    m_vObjectNode[ it->second ].entity    = "";
}
auto Scene::attachLight( std::vector<PointLight> const &pointlight ) -> void
{
    m_PointLight = pointlight;
}
auto Scene::attachLight( std::vector<SpotLight> const &spotlight ) -> void
{
    m_SpotLight = spotlight;
}
auto Scene::attachLight( DirectionalLight const &dlight ) -> void
{
    m_Directional = dlight;
}

void Scene::draw( std::chrono::duration<float, std::chrono::seconds::period> elapsed )
{
    // TODO put back some of this in engine
    m_pShader.Enable();
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    GLuint mvpLocation  = m_pShader.GetUniformLocation( "MVP" );
    GLuint modelviewloc = m_pShader.GetUniformLocation( "modelview" );
    GLuint projectionl  = m_pShader.GetUniformLocation( "projection" );

    // Light
    m_pShader.Init();
    m_pShader.SetLights( m_Directional );
    m_pShader.SetLights( m_PointLight );
    m_pShader.SetLights( m_SpotLight );

    m_pShader.SetEyeWorldPos( m_CameraPosition );

    glUniformMatrix4fv( projectionl, 1, GL_FALSE, glm::value_ptr( m_projection ) );
    // end light
    for ( auto &objectNode : m_vObjectNode )
    {
        if ( objectNode.isVisible )
        {
            glm::mat4 mvp{ 1.f };
            objectNode.DoTransformation( mvp );
            // Send to OpenGL the modelview before apply camera transformation and after object
            // transformation
            glUniformMatrix4fv( modelviewloc, 1, GL_FALSE, glm::value_ptr( mvp ) );
            mvp = m_projection * m_modelview * mvp;
            // send to OpenGL
            glUniformMatrix4fv( mvpLocation, 1, GL_FALSE, glm::value_ptr( mvp ) );
            // send material
            m_pShader.SetMatSpecularIntensity( 1.0 );
            m_pShader.SetMatSpecularPower( 2 );
            // then draw it
            try
            {
                m_meshManager.Draw( objectNode.entity, elapsed, m_pShader, objectNode.animation );
            }
            catch ( std::string &a )
            {
                ResourceExcept re = { objectNode.id, MeshExceptFlag::RELEASE };
                MeshException me;
                me.SetResource( re );
                me.SetMsg( a );
                throw me;
            }
        }
    }
    m_pShader.Disable();
}

auto Scene::registerShader( const std::string &lightVertex, const std::string &lightFragment,
                            const std::string &lightGeometry ) -> void
{
    m_pShader.SetFile( lightVertex, lightFragment, lightGeometry );
}


auto Scene::setNodePosRot( std::string const &entity, glm::vec3 const &pos, glm::vec3 const &pitch )
    -> void
{
    auto it = m_entToID.find( entity );
    if ( it != m_entToID.end() )
    {
        m_vObjectNode[ it->second ].position = pos;
        for ( unsigned int i = 0; i < 3; ++i )
        {
            m_vObjectNode[ it->second ].pitch[ i ] = pitch[ i ];
        }
    }
    // else we do nothing improve performance xD
}
auto Scene::setNodeScale( std::string const &entity, float scale ) -> void
{
    auto it = m_entToID.find( entity );
    if ( it != m_entToID.end() )
        m_vObjectNode[ it->second ].scale = scale;
    // same things like SetObjectPosRot
}
auto Scene::setNodeAnimation( std::string const &entity, std::string const &animation ) -> void
{
    auto it = m_entToID.find( entity );
    if ( it != m_entToID.end() )
        m_vObjectNode[ it->second ].animation = animation;
}
auto Scene::setCameraLocation( glm::vec3 const &pos, glm::vec3 const &center,
                               glm::vec3 const &vert ) -> void
{
    m_modelview      = glm::lookAt( pos, center, vert );
    m_CameraPosition = pos;
    m_CameraCenter   = center;
    m_CameraVertical = vert;
}
auto Scene::setCameraSettings( double fov, double ratio, double near, double far ) -> void
{
    m_projection = glm::perspective( fov, ratio, near, far );
}

void Scene::ObjectNode::DoTransformation( glm::mat4 &mdv ) const
{
    // Translate to position
    mdv = glm::translate( mdv, position );
    // axe  X Y Z orientation
    mdv = glm::rotate( mdv, pitch[ 0 ], glm::vec3( 1, 0, 0 ) );
    mdv = glm::rotate( mdv, pitch[ 1 ], glm::vec3( 0, 1, 0 ) );
    mdv = glm::rotate( mdv, pitch[ 2 ], glm::vec3( 0, 0, 1 ) );
    // resize
    mdv = glm::scale( mdv, glm::vec3( scale, scale, scale ) );
}

} // namespace S3DE
