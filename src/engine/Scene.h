/*
Copyright (c) 2019, Payet Thibault
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Monwarez Inc nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL PAYET THIBAULT BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#pragma once

#include "BasicMeshManager.h"
#include "GBuffer.h"
#include "Light.h"
#include "Mesh.h"
#include "Shader.h"

#include <chrono>
#include <map>
#include <optional>
#include <string>
#include <vector>

namespace S3DE
{
//
class Scene
{
public:
    Scene() = default;

    ///	\brief  Load a bunch of mesh
    ///	\param  meshPair    A vector of entity,filename pair
    auto loadMeshes( std::vector<MeshPair> const& meshPair ) -> void;

    auto delMeshNode( std::string const& entity ) -> void;

    ///	\brief  Set/Update the light information
    ///	\param[in]  pointlight  A vector of PointLight data
    auto attachLight( std::vector<PointLight> const& pointlight ) -> void;

    ///	\brief Set/update the spot light information
    ///	\param	spotlight   a vector of SpotLight data
    auto attachLight( std::vector<SpotLight> const& spotlight ) -> void;

    ///	\brief Set/update the Directional light information
    ///	\param  dlight  a DirectionalLight
    auto attachLight( DirectionalLight const& dlight ) -> void;

    auto draw( std::chrono::duration<float, std::chrono::seconds::period> elapsed ) -> void;

    auto registerShader( std::string const& lightVertex, std::string const& lightFragment,
                         std::string const& lightGeometry ) -> void;



    ///	\brief  Set the position and rotation of an object of the scene
    ///	\param[in]  entity  The name of the entity
    ///	\param[in]  pos The new position
    ///	\param[in]  pitch   The pitch rotation X,Y,Z
    auto setNodePosRot( std::string const& entity, glm::vec3 const& pos, glm::vec3 const& pitch )
        -> void;

    auto setNodeScale( std::string const& entity, float scale ) -> void;

    auto setNodeAnimation( std::string const& entity, std::string const& animation ) -> void;

    auto setCameraLocation( glm::vec3 const& pos, glm::vec3 const& center, glm::vec3 const& vert )
        -> void;

    auto setCameraSettings( double fov, double ratio, double near, double far ) -> void;

private:
    BasicMeshManager m_meshManager;
    std::map<std::string, size_t> m_entToID;
    glm::mat4 m_modelview{ 1.f };
    glm::mat4 m_projection{ 1.f };
    // struct ObjectList
    // Maybe next time we will do SceneGraph
    struct ObjectNode
    {
        std::string entity;
        glm::vec3 position;
        float pitch[ 3 ];
        float scale;
        void DoTransformation( glm::mat4& modelview ) const;
        std::string animation;
        size_t id;      ///< the id of the Mesh
        bool isVisible; ///< Do the node is visible
    };
    auto addMeshNode_( std::string const& entity, bool isVisible = true ) -> void;
    std::vector<ObjectNode> m_vObjectNode;
    // Light is a shader
    Light m_pShader;
    // Here is the different light
    DirectionalLight m_Directional;
    std::vector<PointLight> m_PointLight;
    std::vector<SpotLight> m_SpotLight;
    // Save some camera settings
    glm::vec3 m_CameraPosition;
    glm::vec3 m_CameraCenter;
    glm::vec3 m_CameraVertical;

    std::optional<GBuffer> gbuffer_;
};

} // namespace S3DE
