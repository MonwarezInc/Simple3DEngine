# Local Variables:
# mode: makefile
# End:
# vim: set ft=make :

build_dir := "../build-S3DE"

configure:
	test -e {{build_dir}} || meson {{build_dir}} -Dtest=true -Ddocumentation=true --buildtype=debug

build: configure
	cd {{build_dir}}; ninja

test: build
	cd {{build_dir}}; ninja test

clean:
    rm -r {{build_dir}}
